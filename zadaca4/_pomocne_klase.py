# -*- coding: utf-8 -*-
import gp

# -------------------------- #
#  Pomocne klase             #
# -------------------------- #


class MorfException(Exception):
    pass


class GlasovniIzmjenjivac:

    def __init__(self, gp_spec, gp_skip=()):
        self.gp_spec = gp_spec
        self.static = list(gp.always)
        self.gp_skip = gp_skip

    def morph(self, word, nl, pad=None):
        morph_list = set()
        for gp_list in self._gp_list(pad):
            w = word
            start = len(word) - nl - 1
            for gpID in gp_list:
                if gpID in self.gp_skip:
                    continue
                new = gp.ID[gpID].apply(w, start)
                if new is not None:
                    w = new
                    start += gp.ID[gpID].start or 0
                    if start < 0:
                        raise MorfException(
                            'gp_start manji od 0 za rijec {} kod gp {}'.format(
                                repr(word), repr(gpID)
                                )
                        )
            morph_list.add(w)
        return sorted(list(morph_list))

    def _gp_list(self, pad=None):
        if pad is None:
            return [self.static]
        multi = [[]]
        single = []
        for gp_id, d in self.gp_spec.items():
            if pad in d['pad']:
                if d.get('multi'):
                    multi.insert(0, [gp_id])
                else:
                    single.insert(0, gp_id)
        r = [m + single + self.static for m in multi]
        return r


class Osnova(object):

    def __init__(self, osn, *gp_skip, **kwargs):
        self.osn = osn
        self.osn2 = kwargs.get('osn2')
        self.pad2 = kwargs.get('pad2', '')
        self.gp_skip = gp_skip
        self.nom_nast = kwargs.get('nom_nast')

    def p(self, pad):
        if pad in self.pad2:
            return self.osn2
        else:
            return self.osn

    def copy(self):
        return Osnova(
            self.osn,
            *self.gp_skip,
            osn2=self.osn2,
            pad2=self.pad2,
            nom_nast=self.nom_nast
        )
