# -*- coding: utf-8 -*-
import sys, os
from _pomocne_klase import *


# -------------------------- #
#  Osnovna klasa deklinacije #
# -------------------------- #
class Deklinator(object):

    def __init__(self, rijec, nominativ=None):
        self.dekl_data = dict(
            
            j=dict(nast=self.j_nast, gp=self.j_gp),
            m=dict(nast=self.m_nast, gp=self.m_gp)
        )
        self.rijec = rijec
        self.nominativ = nominativ or rijec

    j = property(lambda self: self.dekliniraj('j'))
    m = property(lambda self: self.dekliniraj('m'))


    def dekliniraj(self, broj):
        dekl = []
        osnove = self.osnove()
        prod_osn = []
        for osn in osnove:
            for onast in self.prod_osnove.keys():
                onast_pad = self.prod_osnove[onast][broj]
                if onast_pad is None:
                    continue
                prod_osn.append(self.prod_osnova(osn, onast, onast_pad))
        osnove.extend(prod_osn)
        for osnova in osnove:
            d = self.dekl_osnove(broj, osnova)
            dekl.append(d)
        return dekl

    def prod_osnova(self, osnova, nast, pad):
        gi = GlasovniIzmjenjivac({})
        gi.static.insert(0, 'Palatalizacija')
        po_list = gi.morph(osnova.osn + nast, len(nast), pad=None)
        if len(po_list) > 1:
            raise MorfException('Produljena osnova ima multi gp')

        prod_osn = osnova.copy()
        prod_osn.osn2 = po_list[0]
        prod_osn.pad2 = pad or ''
        return prod_osn

    def dekl_osnove(self, broj, osnova):
        gp_spec = self.dekl_data[broj]['gp']
        nastavci = self.dekl_data[broj]['nast']
        dekl = {}
        gi = GlasovniIzmjenjivac(gp_spec, osnova.gp_skip)
        for pad in 'ngdali':
            if broj == 'j' and pad == 'n' and self.nominativ:
                pad_list = dekl.setdefault(pad, [])
                if self.nominativ not in pad_list:
                    pad_list.append({'imenica':self.nominativ})

            for nast in nastavci[pad]:
                spojeno = osnova.p(pad) + nast
                nast_len = len(nast)
                im_list = gi.morph(spojeno, nast_len, pad)
#                 im_dict = dict(imenica = gi.morph(spojeno, nast_len, pad), nastavak = nast, osnova = osnova.p(pad))
                pad_list = dekl.setdefault(pad, [])
#                 for i in im_list:
#                     if i not in pad_list:
#                         pad_list.append(i)
#                 for i in im_dict["imenica"]:
                for i in im_list:
                    try:
                        if not any(i in imenice["imenica"] for imenice in pad_list):
                            pad_list.append(dict(imenica = i, nastavak = nast, osnova = osnova.p(pad)))
                    except:
                        pad_list.append(dict(imenica = i, nastavak = nast, osnova = osnova.p(pad)))
        # return dict(osn=osnova.osn2 or osnova.osn, broj=broj, dekl=dekl)
#         dekl["osnova"] = dict( gp_skip = osnova.gp_skip, osn = osnova.osn, osn2 = osnova.osn2, pad2 = osnova.pad2)
        return dekl
    

# ---------------------------- #
#  Deklinacije s obzirom je li zadnje slovo tvrdo ili meko                #
# ---------------------------- #
#  m   MuskiTvrdi,MuskiMeki    #
#  z   ZenskiTvrdi,ZenskiMeki  #
#  s   SrednjiTvrdi,SrednjiMeki#
# ---------------------------- #

class MuskiTvrdi(Deklinator):

    rod = 'm'
    j_nast = {
        'n': [''],
        'g': ['a'],
        'd': ['u'],
        'a': ['', 'a'],
        'l': ['1'],
        'i': ['om']
    }
    m_nast = {
        'n': ['4','a'],
        'g': ['ov'],
        'd': ['am'],
        'a': ['4','ov'],
        'l': ['ah'],
        'i': ['ami']
    }
    prod_osnove = {'ov': dict(j=None, m='ngdavli'),
                   'ev': dict(j=None, m='ngdavli')}
    j_gp = {
        'Palatalizacija': dict(pad='ngdavli'),
        'Jotacija': dict(pad='ngdavli')
    }
    m_gp = {
        'Palatalizacija': dict(pad='ngdavli'),
        'Jotacija': dict(pad='ngdavli')
    }

    def osnove(self):
        nominativ = self.nominativ
        osnove = []
        osn = nominativ[:-1]
        nom_nast = nominativ[-1]

        if nominativ.endswith(('o', 'a')):
            osnove.append(Osnova(osn, nom_nast=nom_nast))
        else:
            osnove.append(Osnova(nominativ))

        return osnove


class MuskiMeki(Deklinator):

    rod = 'm'
    j_nast = {
        'n': [''],
        'g': ['6'],
        'd': ['5'],
        'a': ['', '6'],
        'l': ['1'],
        'i': ['1m']
    }
    m_nast = {
        'n': ['i','6'],
        'g': ['1j'],
        'd': ['6m'],
        'a': ['i','6'],
        'l': ['6h'],
        'i': ['6mi']
    }
    prod_osnove = {'ov': dict(j=None, m='ngdavli'),
                   'ev': dict(j=None, m='ngdavli')}
    j_gp = {
        'Palatalizacija': dict(pad='ngdavli'),
        'Jotacija': dict(pad='ngdavli')
    }
    m_gp = {
        'Palatalizacija': dict(pad='ngdavli'),
        'Jotacija': dict(pad='ngdavli')
    }

    def osnove(self):
        nominativ = self.nominativ
        osnove = []
        osn = nominativ[:-1]
        nom_nast = nominativ[-1]

        if nominativ.endswith(('o', 'a')):
            osnove.append(Osnova(osn, nom_nast=nom_nast))
        else:
            osnove.append(Osnova(nominativ))

        return osnove
class SrednjiTvrdi(Deklinator):

    rod = 's'
    j_nast = {
        'n': ['o'],
        'g': ['a'],
        'd': ['u'],
        'a': ['o'],
        'l': ['1'],
        'i': ['om']
    }
    m_nast = {
        'n': ['4','a'],
        'g': [''],
        'd': ['am'],
        'a': ['4','ov'],
        'l': ['ah'],
        'i': ['ami']
    }
    prod_osnove = {'ov': dict(j=None, m='ngdavli'),
                   'ev': dict(j=None, m='ngdavli')}
    j_gp = {
        'Palatalizacija': dict(pad='ngdavli'),
        'Jotacija': dict(pad='ngdavli')
    }
    m_gp = {
        'Palatalizacija': dict(pad='ngdavli'),
        'Jotacija': dict(pad='ngdavli')
    }

    def osnove(self):
        nominativ = self.nominativ
        return [Osnova(nominativ[:-1], nom_nast=nominativ[-1])]


class SrednjiMeki(Deklinator):

    rod = 's'
    j_nast = {
        'n': ['1'],
        'g': ['6'],
        'd': ['5'],
        'a': ['1'],
        'l': ['1'],
        'i': ['1m']
    }
    m_nast = {
        'n': ['i','6'],
        'g': ['ej'],
        'd': ['6m'],
        'a': ['i','6'],
        'l': ['6h'],
        'i': ['6mi']
    }
    prod_osnove = {'ov': dict(j=None, m='ngdavli'),
                   'ev': dict(j=None, m='ngdavli')}
    j_gp = {
        'Palatalizacija': dict(pad='ngdavli'),
        'Jotacija': dict(pad='ngdavli')
    }
    m_gp = {
        'Palatalizacija': dict(pad='ngdavli'),
        'Jotacija': dict(pad='ngdavli')
    }
    def osnove(self):
        nominativ = self.nominativ
        return [Osnova(nominativ[:-1], nom_nast=nominativ[-1])]




class ZenskiMeki(Deklinator):

    rod = 'z'
    j_nast = {
        'n': ['6'],
        'g': ['i'],
        'd': ['1'],
        'a': ['5'],
        'l': ['1'],
        'i': ['1j']
    }
    m_nast = {
        'n': ['i','6'],
        'g': [''],
        'd': ['6m'],
        'a': ['i','6'],
        'l': ['6h'],
        'i': ['6mi']
    }
    prod_osnove = {}
    j_gp = {
        'Palatalizacija': dict(pad='ngdavli'),
        'Jotacija': dict(pad='ngdavli')
    }
    m_gp = {
        'Palatalizacija': dict(pad='ngdavli'),
        'Jotacija': dict(pad='ngdavli')
    }
    def osnove(self):
        nominativ = self.nominativ
        return [Osnova(nominativ[:-1], nom_nast=nominativ[-1])]

class ZenskiTvrdi(Deklinator):

    rod = 'z'
    j_nast = {
        'n': ['a'],
        'g': ['4'],
        'd': ['1'],
        'a': ['u'],
        'l': ['1'],
        'i': ['o5']
    }
    m_nast = {
        'n': ['4','a'],
        'g': [''],
        'd': ['am'],
        'a': ['4','ov'],
        'l': ['ah'],
        'i': ['ami']
    }
    prod_osnove = {}
    j_gp = {
        'Palatalizacija': dict(pad='ngdavli'),
        'Jotacija': dict(pad='ngdavli')
    }
    m_gp = {
        'Palatalizacija': dict(pad='ngdavli'),
        'Jotacija': dict(pad='ngdavli')
    }

    def osnove(self):
        nominativ = self.nominativ
        return [Osnova(nominativ[:-1], nom_nast=nominativ[-1])]


class DeklinacijaImenice(object):

    def __init__(self, nominativ, rod, dekl):
        dekl_obj = self._deklinacije(rod, dekl)
        dekl = dekl_obj(nominativ, nominativ)
        self.dekl = dict(j=dekl.j, m=dekl.m)

    def _deklinacije(self, rod, dekl):
        if rod == 'm':
            try:
                return {'tvrdi':MuskiTvrdi,'meki':MuskiMeki}[dekl]
            except KeyError:
                raise MorfException(
                        'Imenica: dekl za rod "m" mora biti "0", "oe"'
                        )
        elif rod == 'z':
            try:
                return {'tvrdi': ZenskiTvrdi, 'meki': ZenskiMeki}[dekl]
            except KeyError:
                raise MorfException(
                        'Imenica: dekl za rod "z" mora biti "0", "a"'
                        )
        elif rod == 's':
            try:
                return {'tvrdi':SrednjiTvrdi,'meki':SrednjiMeki}[dekl]
            except KeyError:
                raise MorfException(
                        'Imenica: dekl za rod "m" mora biti "0", "oe"')
        else:
            raise MorfException('Imenica: rod mora biti "mzs"')
  

#Pretvaranje ćirilice u latinicu i obrnuto s tim da su neki simboli posebni          
symbols = (u"абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ",
           u"abvgd12žzijklmnoprstufhcčš3_4_e56ABVGD78ŽZIJKLMNOPRSTUFHCČŠ9_%_EUA")
#je ->1
#jo ->2
#šč ->3
#I -> 4
#ju -> 5
#ja -> 6
#JE ->7
#JO ->8
#ŠČ -> 9
#Jako veliko I -> %

c2l = {ord(a):ord(b) for a, b in zip(*symbols)}
l2c = {ord(b):ord(a) for a, b in zip(*symbols)}



if __name__=="__main__":
    
    from pprint import pprint
    #muski tvrdi стол -> stol
    #rijec = "стол"
    #q = DeklinacijaImenice(rijec.translate(c2l),'m','tvrdi')
    #pprint(eval(str(q.dekl).translate(l2c)))
    #pprint(q.dekl) #-> ispis u latinici
    
    #muski meki герой -> junak
    rijec = "герой"
    q = DeklinacijaImenice(rijec.translate(c2l),'m','meki')
    pprint(eval(str(q.dekl).translate(l2c)))
    pprint(q.dekl) #-> ispis u latinici
    

    #zenski tvrdi деревня -> selo
    #rijec = "деревня"
    #q = DeklinacijaImenice(rijec.translate(c2l),'z','tvrdi')
    #pprint(eval(str(q.dekl).translate(l2c)))
    #pprint(q.dekl) #-> ispis u latinici
    
    #zenski meki 	линия -> linija
    #rijec = "линия"
    #q = DeklinacijaImenice(rijec.translate(c2l),'z','meki')
    #pprint(eval(str(q.dekl).translate(l2c)))
    #pprint(q.dekl) -> ispis u latinici
    
          
    #srednji tvrdi окно -> prozor
    #rijec = "окно"
    #q = DeklinacijaImenice(rijec.translate(c2l),'s','tvrdi')
    #pprint(eval(str(q.dekl).translate(l2c)))
    #pprint(q.dekl) -> ispis u latinici
    
    #srednji meki 	ружьё -> puška
    #rijec = "ружьё"
    #q = DeklinacijaImenice(rijec.translate(c2l),'s','meki')
    #pprint(eval(str(q.dekl).translate(l2c)))
    #pprint(q.dekl) -> ispis u latinici
    
    
