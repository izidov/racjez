# -*- coding: utf-8 -*-
import re

# ---------------------------------------------- #


class GlasovnaPromjena:
    ID = None
    start = None

    def __init__(self):
        pass

    def apply(self, word):
        raise NotImplementedError('%s.apply()' % (self))


def _dmatch(d):
    @staticmethod
    def _sub(m):
        return d[m.group()]
    return _sub

# ---------------------------------------------- #
#             SAMOGLASNICKE PROMJENE             #
# ---------------------------------------------- #


# ---------------------------------------------- #
#              SUGLASNICKE PROMJENE              #
# ---------------------------------------------- #
class _Palatalizacija(GlasovnaPromjena):
    ID = 'Palatalizacija'
    apply_re = re.compile('[kgh](?=[1ei])', re.I)
    apply_d = _dmatch(
       {
            u'k': u'č',
            u'g': u'ž',
            u'h': u'š'
        }
    )
    start = -1

    def apply(self, word, start=0):
        if self.apply_re.search(word, start):
            r = self.apply_re.sub(self.apply_d, word[start:])
            return word[:start] + r
        return None



class _Jotacija(GlasovnaPromjena):
    ID = 'Jotacija'
    apply_re = re.compile('[dghktsz]j', re.I)
    apply_re1 = re.compile('[dghktsz]1', re.I)
    apply_re2 = re.compile('[dghktsz]2', re.I)
    apply_re3 = re.compile('skj', re.I)
    apply_re5 = re.compile('[dghktsz]5', re.I)
    apply_re6 = re.compile('[dghktsz]6', re.I)
    apply_d = _dmatch(
        {
            u'dj': u'ž',
            u'gj': u'ž',
            u'hj': u'š',
            u'kj': u'č',
            u'sj': u'š',
            u'tj': u'č',
            u'zj': u'ž',
            
            
        }
    )
    apply_d1 = _dmatch(
        {
            u'd1': u'ž1',
            u'g1': u'ž1',
            u'h1': u'š1',
            u'k1': u'č1',
            u's1': u'š1',
            u't1': u'č1',
            u'z1': u'ž1',
            
            
        }
    )
    apply_d2 = _dmatch(
        {
            u'd2': u'ž2',
            u'g2': u'ž2',
            u'h2': u'š2',
            u'k2': u'č2',
            u's2': u'š2',
            u't2': u'č2',
            u'z2': u'ž2',
        
            
        }
    )
    apply_d5 = _dmatch(
        {
            u'd5': u'ž5',
            u'g5': u'ž5',
            u'h5': u'š5',
            u'k5': u'č5',
            u's5': u'š5',
            u't5': u'č5',
            u'z5': u'ž5',
            
            
        }
    )
    apply_d6 = _dmatch(
        {
            u'd6': u'ž6',
            u'g6': u'ž6',
            u'h6': u'š6',
            u'k6': u'č6',
            u's6': u'š6',
            u't6': u'č6',
            u'z6': u'ž6',
            
            
        }
    )
    
    start = -1

    def apply(self, word, start=0):
        if self.apply_re.search(word, start):
            r = self.apply_re.sub(self.apply_d, word[start:])
        elif self.apply_re1.search(word, start):
            r = self.apply_re1.sub(self.apply_d1, word[start:])
        elif self.apply_re2.search(word, start):
            r = self.apply_re2.sub(self.apply_d2, word[start:])
        elif self.apply_re5.search(word, start):
            r = self.apply_re5.sub(self.apply_d5, word[start:])
        elif self.apply_re6.search(word, start):
            r = self.apply_re6.sub(self.apply_d6, word[start:])
        elif self.apply_re3.search(word, start):
            r = self.apply_re3.sub(u'3', word[start:])
        else:
            return None
        return word[:start] + r
        return None


# ---------------------------------------------- #
ID = {}

ID[_Palatalizacija.ID] = Palatalizacija = _Palatalizacija()
ID[_Jotacija.ID] = Jotacija = _Jotacija()

# ---------------------------------------------- #
always = (
    'Jotacija',
    'Palatalizacija'
    )
