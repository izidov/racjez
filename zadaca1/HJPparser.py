from bs4 import BeautifulSoup
import requests
import json 

word = input('riječ?')

# HTTP interface 
url = 'http://hjp.znanje.hr/'
payload = {'word':word,'search':'Pretraga'}
result = requests.post(url+'index.php?show=search',data=payload)
content = result.content

# BS4 wrapper
soup = BeautifulSoup(content,"lxml")


entries = soup.find_all('p',{"class":"libersina md"})
import unidecode

def skiniPrijeglase(s):
    string = ""
    for char in s:
        if(char=="č" or char=="ć" or char=="ž" or char=="š" or char=="đ"):
            string = string + char
        else:
            string = string + unidecode.unidecode(char)
    return string
def parser(soup,br=''):
    data_json = {}
    ######### naziv ########
    naziv = soup.find_all('p',{"class":"libersina xl"})
    data_json['naziv']=skiniPrijeglase(naziv[0].text.strip())
    
    ######### opis ########
    opis = soup.find_all('p',{"class":"libersina md"})
    data_json['opis']=opis[0].text.strip()
    
    ######oblici#######
    tablice = []
    oblici = soup.find_all('div',{"id":"izvedeni-oblici"})
    rows = oblici[0].findAll("tr")

    for row in rows:
        cols = row.find_all('td')
        cols = [ele.text.strip() for ele in cols]
        tablice.append([ele for ele in cols if ele])

    newtab = []
    for row in tablice:
        newtab.append(" ".join(row))
    data_json['oblici']=newtab
    
    ######### definicija ########
    definicija = soup.find_all('div',{"id":"definicija"})
    tablice = []
    rows = definicija[0].findAll("tr")

    for row in rows:
        cols = row.find_all('td')
        cols = [ele.text.strip() for ele in cols]
        tablice.append([ele for ele in cols if ele])

    newtab = []
    for row in tablice:
        newtab.append(" ".join(row))
    data_json['definicija']=newtab
    ######### sintagma ########
    sintagma = soup.find_all('div',{"id":"sintagma"})
    data_json['sintagma']=sintagma[0].text.strip()
    
    ######### frazeologija ########
    frazeologija = soup.find_all('div',{"id":"frazeologija"})
    data_json['frazeologija']=frazeologija[0].text.strip()
    
    ######### onomastika ########
    onomastika = soup.find_all('div',{"id":"onomastika"})
    data_json['onomastika']=onomastika[0].text.strip()
    #print(data_json)
    if br==1:
        br = ""
    with open(skiniPrijeglase(naziv[0].text.strip())+br+'.json','w') as outfile: json.dump(data_json,outfile,indent=4)
if len(entries)==0:
    print("Nema podudranja.")
    
if len(entries)==1:
    parser(soup,len(entries))
    
else:
    data = {}
    for s in entries:
        title = s.text.strip().partition(' ')[0] # no HTML tagss
        data[title] = url+s.find("a").attrs['href']
    name = {}
    for key,val in data.items():
        try:
            name[skiniPrijeglase(key)] = name[skiniPrijeglase(key)]+1
        except:
            name[skiniPrijeglase(key)] = 1
        url = val

        #payload = {'word':word,'search':'Pretraga'}
        result = requests.get(url)
        content = result.content

        # BS4 wrapper
        soup = BeautifulSoup(content,"lxml")
        parser(soup,str(name[skiniPrijeglase(key)]))