from bs4 import BeautifulSoup
import json 
import re

soup = BeautifulSoup(open("VRH_pojmovi.htm"), "html.parser")
vrste_riječi={}
vrste_riječi['im m']="Imenica muškog roda"
vrste_riječi['im ž']="Imenica ženskog roda"
vrste_riječi['im sr']="Imenica srednjeg roda"
vrste_riječi['gl']="Glagol"
vrste_riječi['prid']="Pridjev"
vrste_riječi['pril']="Prilog"
vrste_riječi['prij']="Prijedlog"
vrste_riječi['zam']="Zamjenica"
vrste_riječi['vez']="Veznik"
vrste_riječi['glav br']="Glavni broj"
vrste_riječi['vez']="Redni broj"
vrste_riječi['usk']="Usklik"
vrste_riječi['čest']="Čestice"

            
r =soup.find_all('p',{"class":"bodytext20"})

def pars(con):
    rastav = [re.sub(r"\n|\xa0"," ",re.sub(r'(<!--.*?-->|<[^>]*>)', '', x)) for x in con]
    try:
        rastav.remove(" ")
    except:
        pass
    ####### Riječ
    data_json["Riječ: "] = rastav[0]
    rastav.pop(0)
    ####### Vrsta riječi i sređiavnje prefixa
    try:
        data_json["Vrsta riječi: "] = vrste_riječi[rastav[0]]
        rastav.pop(0)
    except:
        data_json["Vrsta riječi: "] = "Prefix"
        pre = ""
        while len(rastav)!=0:
            pre = pre+rastav[0]
            rastav.pop(0)
        data_json["Definicija: "] = pre
        with open(data_json['Riječ: ']+'.json','w') as outfile: json.dump(data_json,outfile,indent=4)
        return data_json,data_json['Riječ: ']
    rastav.pop(0)
    ####### Sklanjanje definirano unutar zagrada
    sklanjanje = ""
    while rastav[0]!="〉":
        sklanjanje = sklanjanje+rastav[0]
        data_json["Sklanjanje: "] = sklanjanje
        rastav.pop(0)
    rastav.pop(0)
    if re.match("\d+\.|(I+\.)",rastav[0]):
        data_json["Definicija: "] = {}
        while re.match("\d+\.|(I+\.)",rastav[0]):
            br = re.match("\d+\.|(I+\.)",rastav[0]).group()
            rastav.pop(0)
            dijelovi = []
            
            while not re.search(r"\d+\.|(I+\.)",rastav[0]):
                dijelovi.append(rastav[0])
                rastav.pop(0)
                if len(rastav)==0:
                    break
            if ("►" in dijelovi or " ► " in dijelovi):
                de = ""
                data_json["Definicija: "][br] = {}
                while (dijelovi[0]!="►"and dijelovi[0]!=" ► "):
                    de = de+dijelovi[0]
                    dijelovi.pop(0)
                dijelovi.pop(0)
                pod = {}
                while dijelovi:
                    if re.match(r"fiz|ekol",dijelovi[0]):
                        dijelovi.pop(0)
                    naz = dijelovi[0]
                    dijelovi.pop(0)
                    pod[naz]=dijelovi[0]
                    dijelovi.pop(0)
                
                data_json["Definicija: "][br][de] = pod
                if len(rastav)==0:
                    break
            else:
                pod = ""
                while dijelovi:
                    if re.search(r"Ⓔ",dijelovi[0]):
                        dijelovi.pop(0)
                        data_json["Dolazi od: "] = dijelovi
                        break
                    pod = pod+dijelovi[0]
                    dijelovi.pop(0)
                    
                data_json["Definicija: "][br] = pod
            if len(rastav)==0:
                    break
    else:
        de = ""
        while rastav:
            if re.search(r"Ⓔ",rastav[0]):
                rastav.pop(0)
                data_json["Dolazi od: "] = rastav
                break
            
            de = de+rastav[0]
            rastav.pop(0)
            if len(rastav)==0:
                break
        data_json["Definicija: "] = de
    
    with open(data_json['Riječ: ']+'.json','w') as outfile: json.dump(data_json,outfile,indent=4)
    return data_json,data_json['Riječ: ']

data_json = {}
ivan = r[33:40]
for rj in ivan:
    data_json = {}
    con = [str(x) for x in rj.contents]
    try:
        dataj,ime = pars(con)
        print(ime)
    except:
        print("neuspjelo")


# Generator za sve - uklonite s CTRL-5
#==============================================================================
# data_json = {}
# ivan = r
# for rj in ivan:
#     data_json = {}
#     con = [str(x) for x in rj.contents]
#     try:
#         dataj,ime = pars(con)
#         print(ime)
#     except:
#         print("neuspjelo")
#==============================================================================
        

