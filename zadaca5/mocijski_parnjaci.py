# -*- coding: utf-8 -*-
"""
Ivan Židov
Mocijski parnjaci - Računalno jezikoslovlje
Siječanj, 2019
"""
filepath = "svi_parnjaci.txt"
muski = []
zenski = []
with open(filepath) as f:
    for line in f:
        inner_list = [elt.strip() for elt in line.split('-')]
        muski.append(inner_list[0])
        zenski.append(inner_list[1])
muski_spol = ["telj","ač","and","ant","ar","ak","aš","at","en","ent","ist","er","graf","or","lac","log","slov","znan","ac","ik","in"]
zenski_spol=["ica","inja","ka","kinja"]
muski_morf = ["","ak","av","iv","jak","jiv","ljiv","eljiv","ov","ev","ijan","in","ant","ist","ac","an","at","čan","ez","in","jan","jak","in"]
komb = [x+y for x in muski_morf for y in muski_spol]
komb.sort()
komb.reverse()
def unos():
    with open(filepath, 'a') as f:
        m = input("Unesite muški parnjak:\n")
        z = input("Unesite ženski parnjak:\n")
        f.write(m+"-"+z+"\n")
        muski.append(m)
        zenski.append(z)
        return
    
def mocijski_parnjak(rijec):
    if rijec in muski:
        ind = muski.index(rijec)
        return(zenski[ind])
    if rijec in zenski:
        ind = zenski.index(rijec)
        return(muski[ind])
    with open(filepath, 'a') as f:
        for sufix in komb:
            if rijec.endswith(sufix):
                if sufix in ["at","graf","ez"]:
                    a = (rijec)+"kinja"
                    f.write(rijec+"-"+a+"\n")
                    muski.append(rijec)
                    zenski.append(a)
                    return(a)
                    break
                if sufix in ["ik"]:
                    a = (rijec[:-2]+"ica")
                    f.write(rijec+"-"+a+"\n")
                    muski.append(rijec)
                    zenski.append(a)
                    return(a)
                    break
                if sufix in ["er","in"]:
                    a = (rijec+"ka")
                    f.write(rijec+"-"+a+"\n")
                    muski.append(rijec)
                    zenski.append(a)
                    return(a)
                    break
                if sufix in ["log","ak","njak"]:
                    a = (rijec+"inja")
                    f.write(rijec+"-"+a+"\n")
                    muski.append(rijec)
                    zenski.append(a)
                    return(a)
                    break
                if sufix in ["znanac","ivac","ovac","evac","ijanac","inac","ac","anac","čanin","čanac"]:
                    a = (rijec[:-2]+"ka")
                    f.write(rijec+"-"+a+"\n")
                    muski.append(rijec)
                    zenski.append(a)
                    return(a)
                    break
                if sufix in ["avac","jivac","ljivac"]:
                    a = (rijec[:-2]+"ica")
                    f.write(rijec+"-"+a+"\n")
                    muski.append(rijec)
                    zenski.append(a)
                    return(a)
                    break
    
                a = (rijec+"ica")
                f.write(rijec+"-"+a+"\n")
                muski.append(rijec)
                zenski.append(a)
                return(a)
    return ("Ne znam kako dobiti parnjak.")
if __name__=="__main__":
    while True:
        print("------------------Traženje mocijskog parnjaka!---------------")
        print("------------------Za prekid unesite - 'stop' ----------------")
        print("----------Za unos nepravilnih parnjaka - 'unos' -------------")
        rijec = input("Unesite riječ:\n")
        if rijec=="stop":
            print("Gotovo")
            break
        if rijec=="unos":
            unos()
            continue
        print(mocijski_parnjak(rijec.lower()))