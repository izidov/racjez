# -*- coding: utf-8 -*-
import sys, os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

import morfolog.gp_remove as gp_remove
from morfolog._pomocne_klase import *


# -------------------------- #
#  Osnovna klasa deklinacije #
# -------------------------- #
class Deklinator(object):

    def __init__(self, rijec, nominativ=None):
        self.dekl_data = dict(
            j=dict(nast=self.j_nast, gp=self.j_gp),
            m=dict(nast=self.m_nast, gp=self.m_gp)
        )
        self.rijec = rijec
        self.nominativ = nominativ or rijec

    j = property(lambda self: self.dekliniraj('j'))
    m = property(lambda self: self.dekliniraj('m'))

    def dekliniraj(self, broj):
        dekl = []
        osnove = self.osnove()
        prod_osn = []
        for osn in osnove:
            for onast in self.prod_osnove.keys():
                onast_pad = self.prod_osnove[onast][broj]
                if onast_pad is None:
                    continue
                prod_osn.append(self.prod_osnova(osn, onast, onast_pad))
        osnove.extend(prod_osn)
        for osnova in osnove:
            d = self.dekl_osnove(broj, osnova)
            dekl.append(d)
        return dekl

    def prod_osnova(self, osnova, nast, pad):
        gi = GlasovniIzmjenjivac({})
        gi.static.insert(0, 'Palatalizacija2')
        po_list = gi.morph(osnova.osn + nast, len(nast), pad=None)
        if len(po_list) > 1:
            raise MorfException('Produljena osnova ima multi gp')

        prod_osn = osnova.copy()
        prod_osn.osn2 = po_list[0]
        prod_osn.pad2 = pad or ''
        return prod_osn

    def dekl_osnove(self, broj, osnova):
        gp_spec = self.dekl_data[broj]['gp']
        nastavci = self.dekl_data[broj]['nast']
        dekl = {}
        gi = GlasovniIzmjenjivac(gp_spec, osnova.gp_skip)
        for pad in 'ngdavli':
            if broj == 'j' and pad == 'n' and self.nominativ:
                pad_list = dekl.setdefault(pad, [])
                if self.nominativ not in pad_list:
                    pad_list.append({'imenica':self.nominativ})

            for nast in nastavci[pad]:
                spojeno = osnova.p(pad) + nast
                nast_len = len(nast)
                im_list = gi.morph(spojeno, nast_len, pad)
#                 im_dict = dict(imenica = gi.morph(spojeno, nast_len, pad), nastavak = nast, osnova = osnova.p(pad))
                pad_list = dekl.setdefault(pad, [])
#                 for i in im_list:
#                     if i not in pad_list:
#                         pad_list.append(i)
#                 for i in im_dict["imenica"]:
                for i in im_list:
                    try:
                        if not any(i in imenice["imenica"] for imenice in pad_list):
                            pad_list.append(dict(imenica = i, nastavak = nast, osnova = osnova.p(pad)))
                    except:
                        pad_list.append(dict(imenica = i, nastavak = nast, osnova = osnova.p(pad)))
        # return dict(osn=osnova.osn2 or osnova.osn, broj=broj, dekl=dekl)
#         dekl["osnova"] = dict( gp_skip = osnova.gp_skip, osn = osnova.osn, osn2 = osnova.osn2, pad2 = osnova.pad2)
        return dekl


# ---------------------------- #
#  Deklinacije                 #
# ---------------------------- #
#  m    Muski0, Muski OE, Ero  #
#  z    ZenskiA, Zenski0       #
#  s    Srednji                #
# ---------------------------- #

class Muski0(Deklinator):
    # a sklonidba
    rod = 'm'
    j_nast = {
        'n': [''],
        'g': ['a'],
        'd': ['u'],
        'a': ['', 'a'],
        'v': ['e', 'u', ''],
        'l': ['u'],
        'i': ['om', 'em']
    }
    m_nast = {
        'n': ['i'],
        'g': ['a'],
        'd': ['ima'],
        'a': ['e'],
        'v': ['i'],
        'l': ['ima'],
        'i': ['ima']
    }
    prod_osnove = {'ov': dict(j=None, m='ngdavli'),
                   'ev': dict(j=None, m='ngdavli')}
    j_gp = {
        'Palatalizacija': dict(pad='v'),
        'NepostojanoA': dict(pad='n')
    }
    m_gp = {
        'Sibilarizacija': dict(pad='ndvli'),
        'NepostojanoA': dict(pad='g')
    }

    def osnove(self):
        nominativ = self.nominativ
        osnove = []
        nepa = gp_remove.nepostojano_a(nominativ)
        vokal = gp_remove.vokalizacija(nominativ)
        if nepa and not vokal:
            osnove.append(Osnova(nepa, 'Vokalizacija'))
        if vokal:
            nepa2 = gp_remove.nepostojano_a(vokal)
            if nepa2:
                osnove.append(Osnova(nepa2))
            else:
                osnove.append(Osnova(vokal, 'NepostojanoA'))
        osnove.append(Osnova(nominativ, 'NepostojanoA', 'Vokalizacija'))
        return osnove


class MuskiOE(Deklinator):
    # a sklonidba
    # bez nastavka u nominativu: dosje, gnu, poni - prebaceni u Muski0
    rod = 'm'
    j_nast = {
        'n': ['o', 'e'],
        'g': ['a'],
        'd': ['u'],
        'a': ['o', 'e', 'a'],
        'v': ['o', 'e'],
        'l': ['u'],
        'i': ['om', 'em']
    }
    m_nast = {
        'n': ['i'],
        'g': ['a'],
        'd': ['ima'],
        'a': ['e'],
        'v': ['i'],
        'l': ['ima'],
        'i': ['ima']
    }
    prod_osnove = {'j': dict(j='gdl', m='ngdavli')}
    j_gp = {'Palatalizacija': dict(pad='v')}
    m_gp = {'Sibilarizacija': dict(pad='ndvli')}

    def osnove(self):
        nominativ = self.nominativ
        osnove = []
        osn = nominativ[:-1]
        nom_nast = nominativ[-1]
        if nominativ.endswith(('o', 'e')):
            osnove.append(Osnova(osn, nom_nast=nom_nast))
        else:
            osnove.append(Osnova(osn))
        return osnove


class Ero(Deklinator):
    # Ero sklonidba
    rod = 'm'
    j_nast = {
        'n': ['o', 'e'],
        'g': ['e'],
        'd': ['i'],
        'a': ['u'],
        'v': ['o'],
        'l': ['i'],
        'i': ['om']
    }
    m_nast = {
        'n': ['e'],
        'g': ['a'],
        'd': ['ama'],
        'a': ['e'],
        'v': ['e'],
        'l': ['ama'],
        'i': ['ama']
    }
    prod_osnove = {}
    j_gp = {'Palatalizacija': dict(pad='v')}
    m_gp = {'Sibilarizacija': dict(pad='ndvli')}

    def osnove(self):
        nominativ = self.nominativ
        osnove = []
        osn = nominativ[:-1]
        nom_nast = nominativ[-1]
        if nominativ.endswith(('o', 'e')):
            osnove.append(Osnova(osn, nom_nast=nom_nast))
        else:
            osnove.append(Osnova(osn))
        return osnove


class Srednji(Deklinator):
    # a sklonidba
    rod = 's'
    j_nast = {
        'n': ['o', 'e'],
        'g': ['a'],
        'd': ['u'],
        'a': ['o', 'e'],
        'v': ['o', 'e'],
        'l': ['u'],
        'i': ['om', 'em']
    }
    m_nast = {
        'n': ['a'],
        'g': ['a'],
        'd': ['ima'],
        'a': ['a'],
        'v': ['a'],
        'l': ['ima'],
        'i': ['ima']
    }
    prod_osnove = {'en': dict(j='gdli', m='ngdavli'),
                   'et': dict(j='gdli', m='ngdavli'),
                   'es': dict(j='gdli', m='ngdavli')}
    j_gp = {}
    m_gp = {'NepostojanoA': dict(pad='g')}

    def osnove(self):
        nominativ = self.nominativ
        osnove = []
        osn = nominativ[:-1]
        nom_nast = nominativ[-1]
        if nominativ.endswith(('o', 'e')):
            osnove.append(Osnova(osn, nom_nast=nom_nast))
        else:
            osnove.append(Osnova(osn))
        return osnove


class ZenskiA(Deklinator):
    # e sklonidba
    rod = 'z'
    j_nast = {
        'n': ['a'],
        'g': ['e'],
        'd': ['i'],
        'a': ['u'],
        'v': ['o', 'e', 'a'],
        'l': ['i'],
        'i': ['om']
    }
    m_nast = {
        'n': ['e'],
        'g': ['a', 'i', 'u'],
        'd': ['ama'],
        'a': ['e'],
        'v': ['e'],
        'l': ['ama'],
        'i': ['ama']
    }
    prod_osnove = {}
    j_gp = {'Sibilarizacija': dict(pad='dl', multi=True)}
    m_gp = {'NepostojanoA': dict(pad='g', multi=True)}

    def osnove(self):
        nominativ = self.nominativ
        return [Osnova(nominativ[:-1], nom_nast=nominativ[-1])]


class Zenski0(Deklinator):
    # i sklonidba
    rod = 'z'
    j_nast = {
        'n': [''],
        'g': ['i'],
        'd': ['i'],
        'a': [''],
        'v': ['i'],
        'l': ['i'],
        'i': ['i', 'ju']
    }
    m_nast = {
        'n': ['i'],
        'g': ['i'],
        'd': ['ima'],
        'a': ['i'],
        'v': ['i'],
        'l': ['ima'],
        'i': ['ima']
    }
    prod_osnove = {}
    j_gp = {'NepostojanoA': dict(pad='na')}
    m_gp = {}

    def osnove(self):
        nominativ = self.nominativ
        osnove = []
        nepa = gp_remove.nepostojano_a(nominativ)
        vokal = gp_remove.vokalizacija(nominativ)
        if nepa and not vokal:
            osnove.append(Osnova(nepa, 'Vokalizacija'))
        if vokal:
            nepa2 = gp_remove.nepostojano_a(vokal)
            if nepa2:
                osnove.append(Osnova(nepa2))
            else:
                osnove.append(Osnova(vokal, 'NepostojanoA'))
        osnove.append(Osnova(nominativ, 'NepostojanoA', 'Vokalizacija'))
        return osnove


# ---------------------------------------------- #

class DeklinacijaImenice(object):

    def __init__(self, nominativ, rod, dekl):
        dekl_obj = self._deklinacije(rod, dekl)
        dekl = dekl_obj(nominativ, nominativ)
        self.dekl = dict(j=dekl.j, m=dekl.m)

    def _deklinacije(self, rod, dekl):
        if rod == 'm':
            try:
                return {'0': Muski0, 'oe': MuskiOE, 'ero': Ero}[dekl]
            except KeyError:
                raise MorfException(
                        'Imenica: dekl za rod "m" mora biti "0", "oe", "Ero"'
                        )
        elif rod == 'z':
            try:
                return {'0': Zenski0, 'a': ZenskiA}[dekl]
            except KeyError:
                raise MorfException(
                        'Imenica: dekl za rod "z" mora biti "0", "a"'
                        )
        elif rod == 's':
            return Srednji
        else:
            raise MorfException('Imenica: rod mora biti "mzs"')
        
        
        

if __name__ == "__main__":
    
    
    from pprint import pprint
    
    q = DeklinacijaImenice('drvo', 's', '0')
    pprint(q.dekl)
    
    
    
