# -*- coding: utf-8 -*-
import re
import os,sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

import morfolog.gp_remove as gp_remove
from morfolog._pomocne_klase import GlasovniIzmjenjivac, Osnova


# -------------------------- #
#   Deklinator               #
# -------------------------- #
class Deklinator(object):
    j_gp = []
    m_gp = []

    def __init__(self, prid, rod=None):
        self.dekl_data = dict(
            j=dict(
                nast=self.j_nast,
                gp=getattr(self, 'j_gp', [])
                ),
            m=dict(
                nast=self.m_nast,
                gp=getattr(self, 'm_gp', [])
                )
        )
        self.prid = prid
        self.rod = rod or 'm'

    j = property(lambda self: self.dekliniraj('j', self.rod))
    m = property(lambda self: self.dekliniraj('m', self.rod))

    def osnove(self):
        prid = self.prid
        osnove = []
        # 1. slucaj - bez nastavka (moguce gp: NepA i Vokalizacija)
        nepa = gp_remove.nepostojano_a(prid)
        vokal = gp_remove.vokalizacija(prid)
        if nepa and not vokal:
            osnove.append(Osnova(nepa, 'Vokalizacija'))
        if vokal:
            nepa2 = gp_remove.nepostojano_a(vokal)
            if nepa2:
                osnove.append(Osnova(nepa2))
            else:
                osnove.append(Osnova(vokal, 'NepostojanoA'))

        # 2. slucaj - nastavci -eg -em -em (gp: Prijeglas - uklonjeno sa nast)
        nast_oe = re.compile('(eg|em|emu)$', re.I)
        if nast_oe.search(prid):
            osn = nast_oe.sub('', prid)
            self.osnove.append(Osnova(osn, 'NepostojanoA', 'Vokalizacija'))

        # 3. slucaj - nema gp
        nast_svi = re.compile(
            '(i|a|o|oga|og|e|oj|omu|om|u|ome|im|ih|ima)$', re.I)
        if nast_svi.search(prid):
            osn = nast_svi.sub(u'', prid)
            osnove.append(
                Osnova(osn, 'NepostojanoA', 'Prijeglas', 'Vokalizacija'))
        else:
            osnove.append(
                Osnova(prid, 'NepostojanoA', 'Prijeglas', 'Vokalizacija'))
        return osnove

    def dekliniraj(self, broj, rod):
        rod_num = dict(m=0, z=1, s=2)[self.rod]
        dekl = []
        for osnova in self.osnove():
            dekl.append(self.dekl_osnove(broj, osnova, rod_num))
#             print dekl
        return dekl

    def dekl_osnove(self, broj, osnova, rod_num):
        dekl = {}
        try:
            gp_spec = self.dekl_data[broj]['gp'][rod_num]
        except IndexError:
            gp_spec = {}
        nast_dict = self.dekl_data[broj]['nast']

        gi = GlasovniIzmjenjivac(gp_spec, osnova.gp_skip)
        gi.static.insert(0, 'Prijeglas')
        for pad in 'ngdavli':
            try:
                nast = nast_dict[pad][rod_num]
            except IndexError:
                nast = nast_dict[pad][0]
            if nast is None:
                continue
            if not isinstance(nast, tuple):
                nast = (nast,)
            for n in nast:
                spojeno = osnova.osn + n
                # ako promijenim da mi se vraca glasovna promjena, onda moram i to malo promijeniti
                prid = dict( pridjev=gi.morph(spojeno, len(n), pad)[0], osnova=osnova.osn, nastavak=n)
                pad_list = dekl.setdefault(pad, [])
                for p in prid["pridjev"]:
                    try:
                        if not any(p in rijec["pridjev"] for rijec in pad_list):
                            pad_list.append(prid)
                    except:
                        pad_list.append(prid)
        return dekl


class Odredeni(Deklinator):
    j_nast = {'n': ['i',  'a',           'o'],
              'g': [('oga', 'og'),  'e', ('oga', 'og')],
              'd': [('omu', 'om'), 'oj', ('omu', 'om')],
              'a': [('oga', 'og', 'i'),  'u',           'o'],
              'v': ['i',  'a',           'o'],
              'l': [('om', 'ome'), 'oj', ('om', 'ome')],
              'i': ['im', 'om',          'im']}

    m_nast = {'n': ['i', 'e', 'a'],
              'g': ['ih'],
              'd': [('im', 'ima')],
              'a': ['e', 'e', 'a'],
              'v': ['i', 'e', 'a'],
              'l': [('im', 'ima')],
              'i': [('im', 'ima')]}


class Neodredeni(Deklinator):
    j_nast = {'n': ['',  'a',  'o'],
              'g': ['a',  'e',  'a'],
              'd': ['u', 'oj',  'u'],
              'a': [('a', ''),  'u',  'o'],
              'v': [None],
              'l': ['u', 'oj',  'u'],
              'i': ['im', 'om', 'im']}

    m_nast = {'n': ['i', 'e', 'a'],
              'g': ['ih'],
              'd': [('im', 'ima')],
              'a': ['e', 'e', 'a'],
              'v': [None],
              'l': [('im', 'ima')],
              'i': [('im', 'ima')]}

    j_gp = [{'NepostojanoA': dict(pad='n')}, {}, {}]


# ---------------------------------------------- #
class DeklinacijaPridjeva(object):

    def __init__(self, pridjev):
        self.prid = pridjev
        tabl = {
            'm': dict(j={}, m={}),
            'z': dict(j={}, m={}),
            's': dict(j={}, m={})
            }
        tabl = {
            'o': dict(m={}, z={}, s={}),
            'n': dict(m={}, z={}, s={})
            }
        o = Odredeni(pridjev)
        n = Neodredeni(pridjev)
        for rod in 'mzs':
            # o.rod = rod
            # n.rod = rod
            for broj in 'jm':
                tabl['o'][rod][broj] = o.dekliniraj(broj, rod)
                tabl['n'][rod][broj] = n.dekliniraj(broj, rod)
        self.dekl = tabl
        
        
        
        
        
if __name__ == "__main__":

    import pprint
    q = DeklinacijaPridjeva('plav')
#     pprint.pprint( q.dekl["o"]["m"]["j"] )
    pprint.pprint( q.dekl )









