# -*- coding: utf-8 -*-
import re
from morfolog.glasovne_konstante import *

# Glasovi
## \N{LATIN SMALL LETTER DZ WITH CARON} \u01C6
## \N{LATIN SMALL LETTER LJ} \u01C9
## \N{LATIN SMALL LETTER NJ} \u01CC

# ---------------------------------------------- #
nepostojano_a_re = re.compile('(?<=[{zat}])a(?=[{zat}]$)'.format(zat=ZATVORNICI), re.I)
def nepostojano_a(word):
    m = nepostojano_a_re.search(word)
    if m:
        return word[:m.start()]+word[m.end():]
    return None

vokalizacija_re = re.compile('o$', re.I)
def vokalizacija(word):
    if vokalizacija_re.search(word):
        return vokalizacija_re.sub(u'l', word)
    return None
