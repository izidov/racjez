# -*- coding: utf-8 -*-
from morfolog.imenice import DeklinacijaImenice as im
from morfolog.pridjevi import DeklinacijaPridjeva as pr
from morfolog.glagoli import TvorbaGlagola as gl
