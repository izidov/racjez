# -*- coding: utf-8 -*-
import re
import sys, os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


import morfolog.gp as gp
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = #
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = #


class GlGlasovniIzmjenjivac:
    def __init__(self, gp_spec, gp_skip=()):
        self.gp_spec = gp_spec
        self.static = [
            'Jotacija',
            'JednacenjeZvucnosti',
            'JednacenjeTvorbe',
            'DvaSlovaZajedno'
            ]
        self.gp_skip = gp_skip

    def morph(self, word, nl, lice=None):
        w = word
        gl_promjena = None
        start = len(word) - nl - 1
        for gpID in self._gp_list(lice):
            if gpID in self.gp_skip:
                continue
            new = gp.ID[gpID].apply(w, start)
            if new is not None:
                w = new
                gl_promjena = gpID
                start += gp.ID[gpID].start or 0
                if start < 0:
                    raise ValueError(
                        'gp_start manji od 0 za rijec {} kod gp {}'.format(
                            repr(word),
                            repr(gpID)
                            )
                        )
        return dict(rijec=w, gl_promjena=gl_promjena)

    def _gp_list(self, lice):
        if lice is None:
            return self.static
        single = []
        for gp_id, gp_lica in self.gp_spec.items():
            if lice in gp_lica:
                single.insert(0, gp_id)
        return 'DvaSlovaZajedno' + single + self.static


class Tvorba(object):
    lica = ['j1', 'j2', 'j3', 'm1', 'm2', 'm3']
    
    def __init__(self, inf, del_skupina=None):
        self.inf = inf
        tvorba = []
        osnove = self.osnove(inf)
        if del_skupina is not None:
            for k in del_skupina:
                try:
                    del osnove[int(k)]
                except IndexError:
                    pass

        for skupina, osn_skup in osnove.items():
            try:
                nast = self.nast[skupina]
            except KeyError:
                continue
            gi = GlGlasovniIzmjenjivac(self.gp_spec.get(skupina, {}))
            for o in osn_skup:
                d = {}
                for k in nast.keys():
                    spojeno = o + nast[k]
                    
                    gl = gi.morph(spojeno, len(nast[k]))
                    
                    d[k] = dict(glagol = gl["rijec"], gl_promjena=gl["gl_promjena"], osnova=o, nastavak=nast[k])
                tvorba.append(d)
        self.tvorba = tvorba


class Prezent(Tvorba):
    oblik = 'p'
    nast = {
        1: dict(j1=u'm', j2=u'š', j3=u'',
                m1=u'mo', m2=u'te', m3=u'ju'),

        2: dict(j1=u'em', j2=u'eš', j3=u'e',
                m1=u'emo', m2=u'ete', m3=u'u'), # <<< PAL

        3: dict(j1=u'im', j2=u'iš', j3=u'i',
                m1=u'imo', m2=u'ite', m3=u'e'), # <<< PAL

        4: dict(j1=u'jem', j2=u'ješ', j3=u'je',
                m1=u'jemo', m2=u'jete', m3=u'ju')
        }
    gp_spec = {2: dict(Palatalizacija=('j1', 'j2', 'j3', 'm1', 'm2', 'm3'))}
    lica = ['j1', 'j2', 'j3', 'm1', 'm2', 'm3']

    def osnove(self, inf):
        osnove = dict()
        # Prva skupina
        if inf.endswith((u'ijevati', u'evati', u'ivati', u'ljati', u'ati')):
            osnove[1] = [inf[:-2]]

        # Druga skupina
        m = re.search(u'rijeti|reti|eti|nuti|uti|psti|sti|ći', inf, re.I)
        if m:
            if m.group() in (u'rijeti', u'reti', u'nuti'):
                osnove[2] = [inf[:m.start()+1]]
            elif m.group() in (u'eti', u'uti'):
                osnove[2] = [inf[:-3] + d for d in ['m', 'n']]
            elif m.group() == u'psti':
                osnove[2] = [inf[:-4] + d for d in ['n', 'b', 'p']]
            elif m.group() == u'sti':
                osnove[2] = [inf[:-3] + d for d in ['s', 'z', 'd', 't']]
            elif m.group() == u'ći':
                osnove[2] = [inf[:-2] + d for d in ['k', 'g', 'h']]

        #Treca skupina
        m = re.search(u'ati|iti', inf, re.I) # fali u'eti'
        if m:
            osnove[3] = [inf[:-3]]

        #cetvrta
        m = re.search('evati|ivati|ovati|uti|vati|ati|ijeti|jeti|iti', inf, re.I)
        if m:
            if m.group() in (u'evati', u'ivati', u'ovati', u'uti'):
                osnove[4] = [inf[:m.start()] + u'u']
            elif m.group() in (u'vati', u'ati'):
                osnove[4] = [inf[:m.start()]]
            elif m.group() in (u'ijeti', u'jeti', u'iti'):
                osnove[4] = [inf[:m.start()] + u'i']

        return osnove


class Imperativ(Prezent):
    oblik = 'm'
    nast = {
        1: dict(j3=u'j', m2=u'jmo', m3=u'jte'),
        2:dict(j3=u'i', m2=u'imo', m3=u'ite'), # <<< SIBI
        3:dict(j3=u'i', m2=u'imo', m3=u'ite'), # <<< SIBI, PALA
        4:dict(j3=u'j', m2=u'jmo', m3=u'jte')
        }
    # DODAO SAM: Palatalizaciju
    gp_spec = {2: dict(Sibilarizacija=('j3', 'm2', 'm3')),
               3: dict(Palatalizacija=('j3', 'm2', 'm3'))}
    lica = ['j3', 'm2', 'm3']


class Aorist(Prezent):
    oblik = 'a'
    nast = {
        1: dict(j1=u'oh', j2=u'e', j3=u'e',
                m1=u'osmo', m2=u'oste', m3=u'oše'),
        2: dict(j1=u'h', j2= u'', j3= u'',
                m1=u'smo', m2=u'ste', m3=u'še')
        }
    gp_spec = {1:dict(Palatalizacija=('j2', 'j3'))}

    def osnove(self, inf):
        osnove = dict()

        if inf.endswith(u'ti'):
            if inf[-3] in u'aeiou':
                osnove.setdefault(2, []).append(inf[:-2])
            else:
                osnove.setdefault(1, []).append(inf[:-2])

        m = re.search('psti|sti|ći', inf, re.I)
        if m:
            if m.group() == u'psti':
                osnove.setdefault(1, []).extend(
                    inf[:-4] + d for d in ['n', 'b', 'p']
                    )
            elif m.group() == u'sti':
                osnove.setdefault(1, []).extend(
                    inf[:-3] + d for d in ['s', 'z', 'd', 't']
                    )
            elif m.group() == u'ći':
                osnove.setdefault(1, []).extend(
                    inf[:-2] + d for d in ['k', 'g', 'h']
                    )
        return osnove


class GlPridjevRadni(Prezent):
    oblik = 'r'
    nast = {
        1: dict(j1=u'ao', j2=u'la', j3=u'lo',
                m1=u'li', m2=u'le', m3=u'la'),
        2: dict(j1=u'o', j2=u'la', j3=u'lo',
                m1=u'li', m2=u'le', m3=u'la')
        }
    gp_spec = {}


class GlPridjevTrpni(Prezent):
    oblik = 't'
    nast = {
        1: dict(j1=u'n', j2=u'na', j3=u'no',
                m1=u'ni', m2=u'ne', m3=u'na'),

        2: dict(j1=u'en', j2=u'ena', j3=u'eno',
                m1=u'eni', m2=u'ene', m3=u'ena'),

        3: dict(j1=u'jen', j2=u'jena', j3=u'jeno',
                m1=u'jeni', m2=u'jene', m3=u'jena'),

        4: dict(j1=u't', j2=u'ta', j3=u'to',
                m1=u'ti', m2=u'te', m3=u'ta'),

        5: dict(j1=u'ven', j2=u'vena', j3=u'veno',
                m1=u'veni', m2=u'vene', m3=u'vena')
        }
    gp_spec = {}

    def osnove(self, inf):
        osnove = dict()
        # Prva skupina
        if inf.endswith(u'ati'):
            osnove[1] = [inf[:-2]]

        # Druga skupina
        m = re.search('uti|psti|sti|ći', inf, re.I)
        if m:
            if m.group() == u'uti':
                osnove[2] = [inf[:-3] + d for d in ['m', 'n']]
            elif m.group() == u'psti':
                osnove[2] = [inf[:-4] + d for d in ['n', 'b', 'p']]
            elif m.group() == u'sti':
                osnove[2] = [inf[:-3] + d for d in ['s', 'z', 'd', 't']]
            elif m.group() == u'ći':
                osnove[2] = [inf[:-2] + d for d in ['k', 'g', 'h']]

        #Treca skupina
        m = re.search('uti|eti|iti', inf, re.I) # fali u'eti'
        if m:
            osnove[3] = [inf[:-2]]

        #Cetvrta skupina
        if inf.endswith((u'nuti', u'uti', u'eti', u'iti')):
            osnove[4] = [inf[:-2]]

        #Peta skupina
        if inf.endswith((u'uti', u'eti', u'iti')):
            osnove[5] = [inf[:-2]]

        return osnove


class GlPrilogSadasnji(Prezent):
    oblik = 's'
    # nast = {
    #     1: dict(j1=u'm', j2=u'š', j3=u'',
    #             m1=u'mo', m2=u'te', m3=u'jući'),

    #     2: dict(j1=u'em', j2=u'eš', j3=u'e',
    #             m1=u'emo', m2=u'ete', m3=u'ući'), # <<< PAL

    #     3: dict(j1=u'im', j2=u'iš', j3=u'i',
    #             m1=u'imo', m2=u'ite', m3=u'eći'), # <<< PAL

    #     4: dict(j1=u'jem', j2=u'ješ', j3=u'je',
    #             m1=u'jemo', m2=u'jete', m3=u'jući')
    #     }

    nast = {
        1: dict(m3=u'jući'),
        2: dict(m3=u'ući'), # <<< PAL
        3: dict(m3=u'eći'), # <<< PAL
        4: dict(m3=u'jući')
        }
    lica = ['m3']


class GlPrilogProsli(Prezent):
    oblik = 'l'
    nast = {1:dict(j1=u'avši'),
            2:dict(j1=u'vši')}
    lica = ['j1']


class Imperfekt(Prezent):
    oblik = 'f'
    nast = {
        1: dict(j1=u'ah', j2=u'aše', j3=u'aše',
                m1=u'asmo', m2=u'aste', m3=u'ahu'),

        2: dict(j1=u'jah', j2=u'jaše', j3=u'jaše',
                m1=u'jasmo', m2=u'jaste', m3=u'jahu'),

        3: dict(j1=u'ijah', j2=u'ijaše', j3=u'ijaše',
                m1=u'ijasmo', m2=u'ijaste', m3=u'ijahu'),

        4: dict(j1=u'ijah', j2=u'ijaše', j3=u'ijaše',
                m1=u'ijasmo', m2=u'ijaste', m3=u'ijahu')
        }
    gp_spec = {3:dict(Sibilarizacija=('j1', 'j2', 'j3', 'm1', 'm2', 'm3'))}

    def osnove(self, inf):
        osnove = dict()
        # Prva skupina
        m = re.search('ati|iti|rijeti|reti|eti|psti', inf, re.I)
        if m:
            if m.group() in (u'ati', u'iti', u'eti'):
                osnove[1] = [inf[:-3]]
            elif m.group() == u'rijeti':
                osnove[1] = [inf[:-5]]
            elif m.group() == u'reti':
                osnove[1] = [inf[:-3]]
            elif m.group() == u'psti':
                osnove[1] = [inf[:-4] + d for d in ['n', 'b', 'p']]

        # Druga skupina
        if inf.endswith(u'nuti'):
            osnove[2] = [inf[:-3]]

        # Treca skupina
        m = re.search('eti|uti|sti|ći', inf, re.I)
        if m:
            if m.group() in (u'eti', u'uti'):
                osnove[3] = [inf[:-3] + d for d in ['m', 'n']]
            elif m.group() == u'sti':
                osnove[3] = [inf[:-3] + d for d in ['s', 'z', 'd', 't']]
            elif m.group() == u'ći':
                osnove[3] = [inf[:-2] + d for d in ['k', 'g', 'h']]

        # Cetvrta skupina
        m = re.search('ijeti|jeti|uti|iti', inf, re.I)
        if m:
            osnove[4] = [inf[:m.start()]]

        return osnove

# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = #

tvorba_dict = {'p':Prezent,
               'a':Aorist,
               'f':Imperfekt,
               'l':GlPrilogProsli,
               's':GlPrilogSadasnji,
               'r':GlPridjevRadni,
               't':GlPridjevTrpni,
               'm':Imperativ}

def tvorba(inf, oblik, skupina=None):
    return tvorba_dict[oblik](inf, skupina)

class TvorbaGlagola(object):
    gl_oblici = {'p':Prezent,
                 'a':Aorist,
                 'f':Imperfekt,
                 'l':GlPrilogProsli,
                 's':GlPrilogSadasnji,
                 'r':GlPridjevRadni,
                 't':GlPridjevTrpni,
                 'm':Imperativ}
    def __init__(self, inf):
        tvorbe = {}
        for oblik, oblikClass in self.gl_oblici.items():
            tvorbe[oblik] = oblikClass(inf).tvorba
        self.tvorbe = tvorbe
        
        
        
        
        
        
if __name__ == "__main__":
    
    import pprint
    

    q = TvorbaGlagola(u'spavati')
    pprint.pprint(q.tvorbe)
#     pprint(q.tvorbe['r'])
        
        
        
        
