# -*- coding: utf-8 -*-

# \N{LATIN SMALL LETTER DZ WITH CARON} \u01C6 ǆ
# \N{LATIN SMALL LETTER LJ} \u01C9 ǉ
# \N{LATIN SMALL LETTER NJ} \u01CC ǌ

OTVORNICI = u'ieaou'

ZVONACNICI = u'jl\u01C9mn\u01CCrv'
SUMNICI = u'bdgptksšzžfhcčć\u01C6đ'
ZATVORNICI = ZVONACNICI + SUMNICI

BEZVUCNI = u'ptkcčćfsšh'
ZVUCNI = u'bdg\u01C6đzž'
ZVUCNId = u'bdg-\u01C6đ-zž-'

ZVUCNI_BEZVUCNI = dict(filter(lambda x: x[0] != u'-', zip(ZVUCNId, BEZVUCNI)))
BEZVUCNI_ZVUCNI = dict(filter(lambda x: x[1] != u'-', zip(BEZVUCNI, ZVUCNId)))
SUMNICI_DICT = ZVUCNI_BEZVUCNI.copy()
SUMNICI_DICT.update(BEZVUCNI_ZVUCNI)

PALATALI = NEPCANICI = u'čć\u01C6đžšj\u01C9\u01CC'

ABCEDA = u'abcčćd\u01C6đefghijkl\u01C9mn\u01CCoprsštuvzž'

OPSTRUENTI = u'ptkbdgcčć\u01C6đfsšhzž'
SONANTI = u'vjl\u01C9rmn\u01CC'
FRIKATIVI = u'fszšžh'
AFRIKATE = u'cčćđ\u01C6'
