# -*- coding: utf-8 -*-
import re
from morfolog.glasovne_konstante import *

# ---------------------------------------------- #


class GlasovnaPromjena:
    ID = None
    start = None

    def __init__(self):
        pass

    def apply(self, word):
        raise NotImplementedError('%s.apply()' % (self))


def _dmatch(d):
    @staticmethod
    def _sub(m):
        return d[m.group()]
    return _sub

# ---------------------------------------------- #
#             SAMOGLASNICKE PROMJENE             #
# ---------------------------------------------- #


class _NepostojanoA(GlasovnaPromjena):
    ID = 'NepostojanoA'
    #apply_re = re.compile(
    #    '[{zat}]{{2}}(?=[{otv}]*$)'.format(
    #        zat=ZATVORNICI, otv=OTVORNICI
    #    ),
    #    re.I
    #)

    def apply(self, word, start=0):
        #m = self.apply_re.search(word)
        #if m:
        #    return word[:m.start() + 1] + 'a' + word[m.end() - 1:]
        return None


class _Vokalizacija(GlasovnaPromjena):
    ID = 'Vokalizacija'
    #apply_re = re.compile('(?<!ije)l$', re.I)

    def apply(self, word, start=0):
        #if self.apply_re.search(word):
        #    return self.apply_re.sub(u'o', word)
        return None


class _Prijeglas(GlasovnaPromjena):
    ID = 'Prijeglas'
    #apply_re = re.compile('(?<=[{nep}])o'.format(nep=NEPCANICI), re.I)

    def apply(self, word, start=0):
    #   if self.apply_re.search(word):
    #        return self.apply_re.sub(u'e', word)
        return None


# ---------------------------------------------- #
#              SUGLASNICKE PROMJENE              #
# ---------------------------------------------- #
class _Palatalizacija(GlasovnaPromjena):
    ID = 'Palatalizacija'
    apply_re = re.compile('[kgh](?=[1ei])', re.I)
    apply_d = _dmatch(
       {
            u'k': u'č',
            u'g': u'ž',
            u'h': u'š'
        }
    )
    start = -1

    def apply(self, word, start=0):
        if self.apply_re.search(word, start):
            r = self.apply_re.sub(self.apply_d, word[start:])
            return word[:start] + r
        return None


class _Palatalizacija2(GlasovnaPromjena):
    ID = 'Palatalizacija2'
#     apply_re = re.compile('c(?=[ei])', re.I)
#     start = -1

    def apply(self, word, start=0):
#         if self.apply_re.search(word, start):
#             r = self.apply_re.sub(u'č', word[start:])
#             return word[:start] + r
        return None


class _Sibilarizacija(GlasovnaPromjena):
    ID = 'Sibilarizacija'
#     apply_re = re.compile('[kgh](?=i)', re.I)
#     apply_d = _dmatch(
#         {
#             u'k': u'c',
#             u'g': u'z',
#             u'h': u's'
#         }
#     )
#     start = 0

    def apply(self, word, start=0):
#         if self.apply_re.search(word, start):
#             r = self.apply_re.sub(self.apply_d, word[start:])
#             return word[:start] + r
        return None


class _Jotacija(GlasovnaPromjena):
    ID = 'Jotacija'
    apply_re = re.compile('[dghktsz](?=[j1256])', re.I)
    apply_re2 = re.compile('skj', re.I)
    apply_d = _dmatch(
        {
            u'dj': u'ž',
            u'gj': u'ž',
            u'hj': u'š',
            u'kj': u'č',
            u'sj': u'š',
            u'tj': u'č',
            u'zj': u'ž',
            u'd1': u'ž1',
            u'g1': u'ž1',
            u'h1': u'š1',
            u'k1': u'č1',
            u's1': u'š1',
            u't1': u'č1',
            u'z1': u'ž1',
            u'd2': u'ž2',
            u'g2': u'ž2',
            u'h2': u'š2',
            u'k2': u'č2',
            u's2': u'š2',
            u't2': u'č2',
            u'z2': u'ž2',
            u'd5': u'ž5',
            u'g5': u'ž5',
            u'h5': u'š5',
            u'k5': u'č5',
            u's5': u'š5',
            u't5': u'č5',
            u'z5': u'ž5',
            u'd6': u'ž6',
            u'g6': u'ž6',
            u'h6': u'š6',
            u'k6': u'č6',
            u's6': u'š6',
            u't6': u'č6',
            u'z6': u'ž6',
            
        }
    )
    start = -1

    def apply(self, word, start=0):
        if self.apply_re.search(word, start):
            r = self.apply_re.sub(self.apply_d, word[start:])
        elif self.apply_re2.search(word, start):
            r = self.apply_re2.sub(u'3', word[start:])
        else:
            return None
        return word[:start] + r
        return None


class _JednacenjeZvucnosti(GlasovnaPromjena):
    ID = 'JednacenjeZvucnosti'
    #apply_re = re.compile('[{sum}]{{2}}'.format(sum=SUMNICI), re.I)
    # start = -1

    def apply(self, word, start=0):
#         m = self.apply_re.search(word, start)
#         if m:
#             prvi = m.group()[0]
#             drugi = m.group()[1]
#             rep = None
#             if prvi in ZVUCNI and drugi in BEZVUCNI:
#                 rep = ZVUCNI_BEZVUCNI[prvi]
#             elif prvi in BEZVUCNI and drugi in ZVUCNI:
#                 rep = BEZVUCNI_ZVUCNI[prvi]
#             if not rep:
#                 return None
#             return word[:m.start()] + rep + word[m.end() - 1:]
        return None


class _JednacenjeTvorbe(GlasovnaPromjena):
    ID = 'JednacenjeTvorbe'
#     apply_re = re.compile('[sz](?=[{nep}])|'
#                           'n(?=[bp])|'
#                           'h(?=[čć])'.format(nep=NEPCANICI), re.I)
#     apply_d = _dmatch(
#         {
#             u's': u'š',
#             u'z': u'ž',
#             u'n': u'm',
#             u'h': u'š'
#         }
#     )
#     start = -1

    def apply(self, word, start=0):
#         if self.apply_re.search(word, start):
#             r = self.apply_re.sub(self.apply_d, word[start:])
#             return word[:start] + r
        return None


class _DvaSlovaZajedno(GlasovnaPromjena):
    ID = 'DvaSlovaZajedno'
#     apply_re = re.compile('(\w)\1', re.I)
#     start = -1

    def apply(self, word, start=0):
#         if self.apply_re.search(word, start):
#             r = self.apply_re.sub('\1', word[start:])
#             return word[:start] + r
        return None


# ---------------------------------------------- #
ID = {}
ID[_NepostojanoA.ID] = NepostojanoA = _NepostojanoA()
ID[_Vokalizacija.ID] = Vokalizacija = _Vokalizacija()
ID[_Prijeglas.ID] = Prijeglas = _Prijeglas()
ID[_Palatalizacija.ID] = Palatalizacija = _Palatalizacija()
ID[_Sibilarizacija.ID] = Sibilarizacija = _Sibilarizacija()
ID[_Jotacija.ID] = Jotacija = _Jotacija()
ID[_JednacenjeZvucnosti.ID] = JednacenjeZvucnosti = _JednacenjeZvucnosti()
ID[_JednacenjeTvorbe.ID] = JednacenjeTvorbe = _JednacenjeTvorbe()
ID[_DvaSlovaZajedno.ID] = DvaSlovaZajedno = _DvaSlovaZajedno()
ID[_Palatalizacija2.ID] = Palatalizacija2 = _Palatalizacija2()
ID[_DvaSlovaZajedno.ID] = DvaSlovaZajedno = _DvaSlovaZajedno()
# ---------------------------------------------- #
always = (
    'Jotacija',
    'JednacenjeZvucnosti',
    'JednacenjeTvorbe'
    )
