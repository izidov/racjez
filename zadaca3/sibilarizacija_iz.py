import re
def _dmatch(d):
    def _sub(m):
        return d[m.group()]
    return _sub

def sibilarizacija(rijec,start=0):
    apply_re = re.compile('[kgh](?=i)|[kgh](?=ijah)|[kgh](?=a)', re.I)
    apply_d = _dmatch(
        {
            u'k': u'c',
            u'g': u'z',
            u'h': u's'
        }
    )
    if apply_re.search(rijec, start):
            r = apply_re.sub(apply_d, rijec[start:])
            return rijec[:start] + r
    return rijec

if __name__=="__main__":
    print(sibilarizacija("ruki"))